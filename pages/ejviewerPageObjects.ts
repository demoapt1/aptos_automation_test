
export class EJViewerPageObjects {

    // selects arrow button of store
    public static get storeList() {
        return browser.element("//*[@class='Select-arrow']");
    }

    // selects the store from the Store    //*[@id="react-select-2--list"]/div[1]/div/div/div[2]
    public static get selectStore() {
         return browser.element("//*[contains(text(),'Chicago, Aptos Store')]");
         // //*[@id="react-select-2--value-item"]
         // //*[@id="react-select-2--value"]/div[1]
         // //*[contains(text(),'Chicago, Aptos Store');
    }

    // clicks on the filterButtonx
    public static get showFilterButton() {
        return browser.element('//button');
    }

    // select the textBox for entering deviceID
    public static get deviceIDTextbox() {
        return browser.element("//*[@placeholder='Enter Device ID...']");
    }
}
