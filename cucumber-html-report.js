var reporter = require('cucumber-html-reporter');

var options = {
    theme: 'bootstrap',
    jsonFile: './report/cucumber.json',
    output: './report/cucumber_report.html',
    reportSuiteAsScenarios: true,
    launchReport: true,
    metadata: {
        "App Version":"NA",
        "Test Environment": "QA",
        "Platform": "Mac",
        "author":"Swati Shikhar"
    }
};

reporter.generate(options);
