"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require('url-search-params-polyfill');
const got = require('got');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const assert = require('assert');
const tunnel = require('tunnel');
let response;
class RestUtils {
    static setStatusCode(statcode) { this._statuscode = statcode; }
    static getStatusCode() { return this._statuscode; }
    static getAccessToken() { return this._accesstoken; }
    static setAccessToken(token) { this._accesstoken = token; }
    get responseData() {
        return this._results;
    }
    doRequest(url, headerOptions, inputObject, formFlag, method) {
        console.log(`URL : ${url} \n REQUEST : ${inputObject.toString()} \n HEADERS : ${headerOptions.toString()}`);
        this._results = got(url, {
            headers: headerOptions.toString(),
            body: inputObject,
            form: formFlag,
            method: method
        });
        response = this._results;
        console.log(`RESPONSE ${response.body} \n STATUS CODE : ${response.statusCode}`);
        return this._results;
    }
    /**
     * Get Method with accessToken
     * @param url
     * @param accesstoken
     */
    getRequestWithAccessToken(url, accesstoken) {
        this._results = got(url, { headers: {
                Authorization: accesstoken,
            },
        });
        return this._results;
    }
    /**
     * Get Method without access Token
     * @param url
     * @param accesstoken
     */
    getRequest(url, accesstoken) {
        if (accesstoken != "") {
            this._results = got(url, { headers: {
                    Authorization: accesstoken,
                },
            });
        }
        else {
            this._results = got(url, { headers: {},
            });
        }
        return this._results;
    }
    /**
     * Post Method for FormBody
     * @param url
     * @param inputbody
     */
    postformbody(url, inputbody) {
        const obj = JSON.parse(inputbody);
        this._results = got(url, {
            body: obj, form: true, headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            method: 'POST',
        });
        return this._results;
    }
    /**
     * Post Method with accessToken
     * @param url
     * @param inputbody
     * @param accesstoken
     */
    postRequest(url, inputbody, accesstoken) {
        console.log(inputbody);
        const obj = JSON.parse(inputbody);
        if (accesstoken != '') {
            this._results = got(url, {
                headers: {
                    'Authorization': accesstoken,
                    'Content-Type': 'application/json',
                },
                method: 'POST',
                body: inputbody
            });
        }
        else {
            this._results = got(url, {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
                body: inputbody
            });
        }
        return this._results;
    }
    /**
     * Put Method with Access Token
     * @param url
     * @param inputbody
     * @param accesstoken
     */
    putbodyWithAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);
        this._results = got(url, {
            headers: {
                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'PUT',
            body: inputbody
        });
        return this._results;
    }
    putRequest(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);
        if (accesstoken != "") {
            this._results = got(url, {
                headers: {
                    'Authorization': accesstoken,
                    'Content-Type': 'application/json',
                },
                method: 'PUT',
                body: inputbody
            });
        }
        else {
            this._results = got(url, {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'PUT',
                body: inputbody
            });
        }
        return this._results;
    }
    patchbodyWithAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);
        this._results = got(url, {
            headers: {
                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'PATCH',
            body: inputbody
        });
        return this._results;
    }
    patchRequest(url, inputbody, accesstoken) {
        console.log(inputbody);
        const obj = JSON.parse(inputbody);
        if (accesstoken != '') {
            this._results = got(url, {
                headers: {
                    'Authorization': accesstoken,
                    'Content-Type': 'application/json',
                },
                method: 'PATCH',
                body: inputbody
            });
        }
        else {
            this._results = got(url, {
                headers: {
                    'Content-Type': 'application/json',
                },
                method: 'POST',
                body: inputbody
            });
        }
        return this._results;
    }
    deleteRequestWithAccessToken(url, accesstoken) {
        this._results = got(url, { headers: {
                Authorization: accesstoken,
            },
        });
        return this._results;
    }
    deleteRequestWithBodyandAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);
        this._results = got(url, {
            headers: {
                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'DELETE',
            body: inputbody
        });
        return this._results;
    }
    deleteRequest(url, accesstoken) {
        if (accesstoken != "") {
            this._results = got(url, { headers: {
                    Authorization: accesstoken,
                },
            });
        }
        else {
            this._results = got(url, { headers: {},
            });
        }
        return this._results;
    }
    validateResponseWithJsonPath(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const json = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log(' key1 :' + key1 + 'value 1 :' + value1);
            if (value1 instanceof Object === false) {
                expect(eval('json.' + key1).toString()).to.equals(value1.toString());
            }
        });
    }
    validateResponse(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        const userStr = JSON.stringify(responsemap);
        console.log('Expected response as json string' + userStr);
        let expectedvalue = false;
        JSON.parse(userStr, (key1, value1) => {
            if (value1 instanceof Object === false) {
                expectedvalue = false;
                JSON.parse(response.body, (key2, value2) => {
                    if (key1 == key2 && value1 == value2) {
                        expectedvalue = true;
                        assert.strictEqual(value1.toString(), value2.toString(), key1 + ' or ' + value1 +
                            ' is not present in response');
                    }
                });
                if (expectedvalue === false) {
                    assert.strictEqual(false, true, key1 + ' or ' + value1 +
                        ' is not present in response');
                }
            }
        });
    }
}
exports.RestUtils = RestUtils;
