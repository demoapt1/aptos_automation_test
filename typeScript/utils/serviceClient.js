"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("./restUtils");
const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const { Given, Then, And, When } = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const RestUtilsobj = new restUtils_1.RestUtils();
var PropertiesReader = require('properties-reader');
// to handle response
let response;
let properties = PropertiesReader('./config/config.properties');
var idmBaseUrl = properties.get('base.idm.url');
var apiBaseUrl = properties.get('base.api.url');
var idmUsername = properties.get('idm.username');
var idmPassword = properties.get('idm.password');
class serviceClient {
    /**
     *  for POST request with out access token
     * @param endPoint
     * @param request
     */
    doPostRequest(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.postRequest(url, request, "");
            console.log(url + '\n');
            console.log(response.body);
            return response;
        });
    }
    /**
     *  POST request using accesstoken in headers
     * @param endPoint
     * @param request
     */
    doPostRequestWithAccessToken(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.postRequest(url, request, restUtils_1.RestUtils.getAccessToken());
            console.log(url + '\n');
            console.log(response.body);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
            return response;
        });
    }
    /**
     *  GET request request with out access token
     * @param endPoint
     * @param request
     */
    doGetRequest(endPoint) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.getRequest(url, "");
            console.log(url + '\n');
            console.log(response.body);
        });
    }
    /**
     *  GET request using accesstoken in headers
     * @param endPoint
     * @param request
     */
    doGetRequestWithAccessToken(endPoint) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.getRequest(url, restUtils_1.RestUtils.getAccessToken());
            console.log(url + '\n');
            console.log(response.body);
        });
    }
    /**
     *  for PUT request with out access token
     * @param endPoint
     * @param request
     */
    doPutRequest(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.putRequest(url, request, "");
            console.log(url + '\n');
            console.log(response.body);
            return response;
        });
    }
    /**
     *  PUT request using accesstoken in headers
     * @param endPoint
     * @param request
     */
    doPutRequestWithAccessToken(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.putRequest(url, request, restUtils_1.RestUtils.getAccessToken());
            console.log(url + '\n');
            console.log(response.body);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
            return response;
        });
    }
    /**
     *  for DELETE request with out access token
     * @param endPoint
     * @param request
     */
    doDeleteRequest(endPoint) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.deleteRequest(url, restUtils_1.RestUtils.getAccessToken());
            console.log(url + '\n');
            console.log(response.body);
        });
    }
    /**
     *  DELETE request using accesstoken in headers
     * @param endPoint
     * @param request
     */
    doDeleteRequestWithAccessToken(endPoint) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.deleteRequest(url, "");
            console.log(url + '\n');
            console.log(response.body);
        });
    }
    /**
     *  for PATCH request with out access token
     * @param endPoint
     * @param request
     */
    doPatchRequest(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.patchRequest(url, request, "");
            console.log(url + '\n');
            console.log(response.body);
            return response;
        });
    }
    /**
     *  PATCH request using accesstoken in headers
     * @param endPoint
     * @param request
     */
    doPatchRequestWithAccessToken(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = apiBaseUrl + endPoint;
            response = yield RestUtilsobj.patchRequest(url, request, restUtils_1.RestUtils.getAccessToken());
            console.log(url + '\n');
            console.log(response.body);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
            return response;
        });
    }
    /**
     * To get Access Token
     * @param endPoint
     * @param request
     */
    getAccessTokenService(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = idmBaseUrl + endPoint;
            var body = request.replace("idm.username", idmUsername).replace("idm.password", idmPassword);
            response = yield RestUtilsobj.postformbody(url, body);
            const json = JSON.parse(response.body);
            console.log("STATUS CODE : " + response.statusCode);
            restUtils_1.RestUtils.setAccessToken(json.access_token);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
        });
    }
    getStatusCode() {
        return __awaiter(this, void 0, void 0, function* () {
            return restUtils_1.RestUtils.getStatusCode().toString();
        });
    }
    getAccessToken(endPoint, request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = idmBaseUrl + endPoint;
            var body = request.replace("idm.username", idmUsername).replace("idm.password", idmPassword);
            const obj = JSON.parse(body);
            let headerMap = new Map();
            headerMap.set('Content-Type', 'application/x-www-form-urlencoded');
            response = yield RestUtilsobj.doRequest(url, headerMap, obj, true, 'POST');
            const json = JSON.parse(response.body);
            restUtils_1.RestUtils.setAccessToken(json.access_token);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
        });
    }
    createUser(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let url = idmBaseUrl + "/users/v2/users";
            let headerMap = new Map();
            headerMap.set('Content-Type', 'application/json');
            response = yield RestUtilsobj.doRequest(url, headerMap, request, false, 'POST');
            const json = JSON.parse(response.body);
            restUtils_1.RestUtils.setStatusCode(response.statusCode.toString());
        });
    }
}
exports.serviceClient = serviceClient;
