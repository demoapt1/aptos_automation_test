"use strict";
/**
 * Step Definitons are the glue code which drive
 * the feature scenarios, Cucumber helps us provide
 * gherkin language syntax's - Given, When, Then
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { Given, When, Then } = require('cucumber');
const calcPage_1 = require("../pages/calcPage");
const calc = new calcPage_1.CalculatorPageObject();
Given(/^I am on my mobile calculator app$/, () => __awaiter(this, void 0, void 0, function* () {
    browser.click('android=new UiSelector().text("Exit").className("android.widget.Button")');
    //  browser.click(Button);
    /*  const title = browser.getText('android.widget.TextView');
      expect(title).to.equal('Calculator');*/
}));
When(/^I add "(.*?)" and "(.*?)"$/, () => __awaiter(this, void 0, void 0, function* () {
    console.log('add');
}));
When(/^I subtract "(.*?)" from "(.*?)"$/, (num1, num2) => {
    /*  browser.click(calc.calcDigitSelector(num1));
      browser.click(calc.subtractOperator);
      browser.click(calc.calcDigitSelector(num2));
      browser.click(calc.equalOperator);*/
});
When(/^I multiply "(.*?)" with "(.*?)"$/, (num1, num2) => {
    /*   browser.click(calc.calcDigitSelector(num1));
       browser.click(calc.multiplyOperator);
       browser.click(calc.calcDigitSelector(num2));
       browser.click(calc.equalOperator);*/
});
When(/^I divide "(.*?)" with "(.*?)"$/, (num1, num2) => {
    /*   browser.click(calc.calcDigitSelector(num1));
       browser.click(calc.divisionOperator);
       browser.click(calc.calcDigitSelector(num2));
       browser.click(calc.equalOperator);*/
});
When(/^I click on AC button$/, () => {
    /*  browser.click(calc.clearOperator);*?
  });
  
  Then(/^the result "(.*?)" should be displayed$/, (result: string) => {
     /* return expect(browser.getText(calc.outputText)).to.contain(result);*/
});
Then(/^the result should be cleared$/, () => {
    /*  return expect(browser.getText(calc.outputText)).to.equal('');*/
});
