"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
const assert = require('assert');
const { Given, Then, And, When } = require('cucumber');
const queryString = require('query-string');
const RestUtilsobj = new restUtils_1.RestUtils();
let response;
Given(/^send POST request to "([^"]*)", input body is$/, (url, inputbody) => __awaiter(this, void 0, void 0, function* () {
    response = yield RestUtilsobj.postformbody(url, inputbody);
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    restUtils_1.RestUtils.setAccessToken(json.access_token);
    console.log('ACCESS TOKEN' + restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response);
    restUtils_1.RestUtils.setStatusCode(response.statusCode);
}));
Then(/^the status code must be (.*)$/, (statusCode) => __awaiter(this, void 0, void 0, function* () {
    assert.strictEqual(restUtils_1.RestUtils.getStatusCode().toString(), statusCode.toString(), statusCode +
        'Status code is not equal' + restUtils_1.RestUtils.getStatusCode());
}));
