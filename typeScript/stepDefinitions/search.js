"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
//import { browser, protractor } from "protractor";
const searchPage_1 = require("../pages/searchPage");
//import { RestUtil} from "../utils/restUtil";
const cucumber_1 = require("cucumber");
//import { HttpClient } from "protractor-http-client";
// tslint:disable-next-line:no-import-side-effect
//import "jasmine";
//import { STATUS_CODES } from "http";
//import assert = require('assert');
//import * as restm from 'typed-rest-client/RestClient';
//import * as util from 'typed-rest-client/Util';
//import * as fs from 'fs';
//import * as path from 'path';
//mport * as httpm from 'typed-rest-client/HttpClient';
//import * as hm from 'typed-rest-client/Handlers';
//import * as chai from 'chai';
//import chaiHttp = require('chai-http');
//var chakram = require('chakram');
const { When, Then } = require("cucumber");
const search = new searchPage_1.SearchPageObject();
//const restobj: RestUtil = new RestUtil();
//var defered = protractor.promise.defer();
//restobj.disp();
When(/^I type "(.*?)"$/, (text) => __awaiter(this, void 0, void 0, function* () {
    search.open();
    yield search.searchTextBox.setValue(text);
}));
When(/^I click on search button$/, () => __awaiter(this, void 0, void 0, function* () {
    yield browser.click();
}));
Then(/^I click on google logo$/, () => __awaiter(this, void 0, void 0, function* () {
    yield search.logo.click();
}));
cucumber_1.Given(/^get rest call$/, () => __awaiter(this, void 0, void 0, function* () {
    /* let _rest: restm.RestClient;
     let _restBin: restm.RestClient;
 
 
         _rest = new restm.RestClient('typed-rest-client-tests');
      //   _restBin = new restm.RestClient('typed-rest-client-tests', 'https://httpbin.org');
        // this.timeout(3000);
 
         let restRes: restm.IRestResponse<HttpBinData> = await _rest.get<HttpBinData>('https://www.google.com');
         
     //   console.log("RESPONSE"+ restRes.result.json);
         assert(restRes.statusCode == 200, "statusCode should be 200");
         //assert(restRes.result && restRes.result.url === 'https://httpbin.org/get');
         let _http: httpm.HttpClient;
         let _httpbin: httpm.HttpClient;
 
       //  let res: httpm.HttpClientResponse = await _http.get('http://httpbin.org/get');
      //   assert(res.message.statusCode == 200, "status code should be 200");
        // let body: string = await res.readBody();
         //let obj:any = JSON.parse(body);
        // console.log(obj)
        
 */
    //await restobj.disp();
    //let resp = chakram.get("http://httpbin.org/get");
    //console.log(resp);
}));
