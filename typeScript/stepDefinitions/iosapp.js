"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { Given, When, Then } = require('cucumber');
// import {CalculatorPageObject} from '../pages/calcPage';
// import { threadId } from 'worker_threads';
// const calc: CalculatorPageObject = new CalculatorPageObject();
Given(/^WebdriverIO and Appium, when interacting with a login form$/, () => __awaiter(this, void 0, void 0, function* () {
    $('~Login').click();
}));
When(/^should be able login successfully$/, () => __awaiter(this, void 0, void 0, function* () {
    //   await new Promise(resolve => setTimeout(()=>resolve(), 3000)).then(()=>console.log("fired new"));
    /*  browser.click(calc.calcDigitSelector(num1));
      browser.click(calc.addOperator);
      browser.click(calc.calcDigitSelector(num2));
      browser.click(calc.equalOperator);*/
    yield $('~button-login-container').click();
}));
