"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const { Given, Then, And, When } = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const RestUtilsobj = new restUtils_1.RestUtils();
// to handle response
let response;
