"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const restUtils_1 = require("../../utils/restUtils");
const serviceClient_1 = require("../../utils/serviceClient");
const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const { Given, Then, And, When } = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const RestUtilsobj = new restUtils_1.RestUtils();
const client = new serviceClient_1.serviceClient();
var randomString = require('randomstring');
let response;
Given(/^send POST request to "([^"]*)", input body is$/, (url, inputbody) => __awaiter(this, void 0, void 0, function* () {
    response = yield RestUtilsobj.postformbody(url, inputbody);
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    restUtils_1.RestUtils.setAccessToken(json.access_token);
    console.log('ACCESS TOKEN' + restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response);
    restUtils_1.RestUtils.setStatusCode(response.statusCode);
}));
Given(/^send GET request to "([^"]*)"$/, (url) => __awaiter(this, void 0, void 0, function* () {
    response = yield RestUtilsobj.getRequestWithAccessToken(url, restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
}));
Given(/^send DELETE request to "([^"]*)"$/, (url) => __awaiter(this, void 0, void 0, function* () {
    response = yield RestUtilsobj.deleteRequestWithAccessToken(url, restUtils_1.RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
}));
Given(/^send POST request to application "([^"]*)", input body is$/, (url, inputbody) => __awaiter(this, void 0, void 0, function* () {
    /*response = await RestUtilsobj.postRequest(url, inputbody, RestUtils.getAccessToken());
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    console.log(url + '\n');
    console.log(response);
    RestUtils.setStatusCode(response.statusCode);*/
    response = yield client.doPostRequestWithAccessToken(url, inputbody);
    console.log(response);
}));
Given(/^get access token from "([^"]*)", input body is$/, (url, inputbody) => __awaiter(this, void 0, void 0, function* () {
    response = yield client.getAccessTokenService(url, inputbody);
}));
When(/^I create a user in the application using "([^"]*)",input as$/, (endPoint, request) => __awaiter(this, void 0, void 0, function* () {
    var randomUserName = randomString.generate({
        length: 5,
        charset: 'numeric'
    });
    var body = request.replace("user_name", randomUserName);
    console.log(body);
    response = yield client.doPostRequestWithAccessToken(endPoint, body);
}));
Then(/^the status code will be (.*)$/, (statusCode) => __awaiter(this, void 0, void 0, function* () {
    assert.strictEqual(restUtils_1.RestUtils.getStatusCode(), statusCode.toString(), statusCode +
        'Status code is not equal' + restUtils_1.RestUtils.getStatusCode());
}));
