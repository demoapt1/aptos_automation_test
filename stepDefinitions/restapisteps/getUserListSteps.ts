import {RestUtils} from '../../utils/restUtils';

const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const {Given , Then, And, When} = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;

const RestUtilsobj: RestUtils = new RestUtils();
let response;

Given(/^send GET request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.getRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});

Then(/^user response includes the following$/, async (expectedresponse) => {
    RestUtilsobj.validateResponse(await RestUtilsobj.responseData, expectedresponse);

});

Then(/^response includes the following$/, async ( responseFields ) => {
    RestUtilsobj.validateResponseWithJsonPath(await RestUtilsobj.responseData, responseFields);

});