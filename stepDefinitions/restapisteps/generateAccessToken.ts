import {RestUtils} from '../../utils/restUtils';

const assert = require('assert');
const { Given , Then, And, When} = require('cucumber');
const queryString = require('query-string');
const RestUtilsobj: RestUtils = new RestUtils();
let response;

Given(/^send POST request to "([^"]*)", input body is$/, async (url, inputbody) => {
    response = await RestUtilsobj.postformbody(url, inputbody);
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    RestUtils.setAccessToken(json.access_token);
    console.log('ACCESS TOKEN' + RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response);
    RestUtils.setStatusCode(response.statusCode);
});

Then(/^the status code must be (.*)$/, async (statusCode) => {
        assert.strictEqual(RestUtils.getStatusCode().toString(), statusCode.toString(), statusCode +
            'Status code is not equal' + RestUtils.getStatusCode() );
 });
