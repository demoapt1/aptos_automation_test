import {RestUtils} from '../../utils/restUtils';
import {serviceClient} from "../../utils/serviceClient";

const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const {Given , Then, And, When} = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const RestUtilsobj: RestUtils = new RestUtils();
const client:serviceClient = new serviceClient();
var randomString = require('randomstring');
let response;

/*
Given(/^send GET request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.getRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});

Given(/^send DELETE request to "([^"]*)"$/, async (url) => {
    response = await RestUtilsobj.deleteRequestWithAccessToken(url, RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response.body);
});
*/

/**
 *To Get access Token
 */
Given(/^get access token from "([^"]*)", input body is$/, async (url, inputbody ) => {
    response = await client.getAccessToken(url,inputbody);
});

/**
 * For Create User
 */
When(/^I create a user in the application by using input as$/,async (request) =>{
    var randomUserName = randomString.generate({
        length: 5,
        charset: 'numeric'
    });
    var body = request.replace("user_name",randomUserName);
    response = await client.createUser(body);
});

/**
 * To check Status code
 */
Then(/^the status code will be (.*)$/,async (statusCode) =>{
    assert.strictEqual(RestUtils.getStatusCode(), statusCode.toString(), statusCode +
        'Status code is not equal' + RestUtils.getStatusCode() );
});
/**
 * To get user List
 */
When(/^I get users list$/,async() =>{
    response = await client.getUserList();
});

