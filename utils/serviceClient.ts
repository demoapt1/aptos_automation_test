import {RestUtils} from "./restUtils";

const got = require('got');
const assert = require('assert');
const tunnel = require('tunnel');
const request = require('then-request');
const {Given , Then, And, When} = require('cucumber');
const queryString = require('query-string');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const RestUtilsobj: RestUtils = new RestUtils();
var PropertiesReader = require('properties-reader');

let response;
let properties = PropertiesReader('./config/config.properties');
var idmBaseUrl = properties.get('base.idm.url');
var apiBaseUrl = properties.get('base.api.url');
var idmUsername = properties.get('idm.username');
var idmPassword = properties.get('idm.password');

export class serviceClient{


    /**
     * To get Access Token
     * @param endPoint
     * @param request
     */
    /*
    public async getAccessTokenService(endPoint,request){
        let url = idmBaseUrl+endPoint;
        var body = request.replace("idm.username",idmUsername).replace("idm.password",idmPassword);
        response = await RestUtilsobj.postformbody(url,body);
        const json = JSON.parse(response.body);
        console.log("STATUS CODE : "+response.statusCode);
        RestUtils.setAccessToken(json.access_token);
        RestUtils.setStatusCode(response.statusCode.toString());
    }
    */
    public async getStatusCode(){
        return RestUtils.getStatusCode().toString();
    }

    /**
     *  to get access token from the access token service
     * @param endPoint
     * @param request
     */
    public async getAccessToken(endPoint,request){
        let url = idmBaseUrl+endPoint;
        var body = request.replace("idm.username",idmUsername).replace("idm.password",idmPassword);
        const obj = JSON.parse(body);
        let headerMap = new Map();
        headerMap.set('Content-Type','application/x-www-form-urlencoded');
        response = await RestUtilsobj.doRequest(url,headerMap,obj,true,'POST');
        const json = JSON.parse(response.body);
        RestUtils.setAccessToken(json.access_token);
        RestUtils.setStatusCode(response.statusCode.toString());
    }

    /**
     * to create user
     * @param request
     */
    public async createUser(request){
        let url = apiBaseUrl+"/users/v2/users";
        var accessToken = RestUtils.getAccessToken();
        var headers = {
            Authorization : accessToken,
            'Content-Type':'application/json'
        }
        response = await RestUtilsobj.doRequest(url,headers,request,false,'POST');
        const json = JSON.parse(response.body);
        RestUtils.setStatusCode(response.statusCode.toString());
    }

    /**
     *  To Get list of users
     */
    public async getUserList(){
        let url = apiBaseUrl+"/users/v2/users";
        var accessToken = RestUtils.getAccessToken();
        var headers = {
            Authorization : accessToken,
            'Content-Type':'application/json'
        }
        response = await RestUtilsobj.doRequest(url,headers,'',false,'GET');
        const json = JSON.parse(response.body);
        RestUtils.setStatusCode(response.statusCode.toString());
    }




}



