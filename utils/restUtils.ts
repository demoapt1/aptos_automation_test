 require('url-search-params-polyfill');
const got = require('got');
const chai = require('chai').use(require('chai-as-promised'));
const expect = chai.expect;
const assert = require('assert');
const tunnel = require('tunnel');

let response;

export class RestUtils {

    public static _statuscode: string;
    public static setStatusCode(statcode): void { this._statuscode = statcode;  }
    public static getStatusCode(): string {  return this._statuscode; }
    public static getAccessToken(): string { return  this._accesstoken; }
    public static setAccessToken(token): void { this._accesstoken = token; }
    private static _accesstoken: any;
    public  _results: any ;


    public get responseData(): any { return this._results;
    }

    // service client to get response
    public async doRequest(url,headerOptions,inputObject,formFlag,method){
        console.log("=============================================================================================")
        console.log('URL : '+url+'\nREQUEST : '+inputObject.toString()+'\n HEADERS : '+headerOptions.toString());
        this._results = got(url,{
            headers : headerOptions,
            body:inputObject,
            form:formFlag,
            method:method
        });
        response = await this._results;
        console.log(`RESPONSE ${response.body} \n STATUS CODE : ${response.statusCode}`);

        return this._results;
    }

    /**
     * Get Method with accessToken
     * @param url
     * @param accesstoken
     */

    public  getRequestWithAccessToken(url, accesstoken) {
        this._results =  got(url, {  headers: {

                Authorization: accesstoken,
            },
        });
        return this._results;

    }

    /**
     * Post Method with FormBody
     * @param url
     * @param inputbody
     */
    public  postformbody(url, inputbody) {
        const obj = JSON.parse(inputbody);
        this._results = got( url, {
             body: obj, form: true, headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            method: 'POST',
        } );
        return this._results;
    }

    /**
     * Post Method with accessToken
     * @param url
     * @param inputbody
     * @param accesstoken
     */
    public  postBodyWithAccessToken(url, inputbody , accesstoken) {
        const obj = JSON.parse(inputbody);

        this._results = got( url, {
            headers: {

                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'PUT',
            body: inputbody
        } );
        return this._results;
    }

    /**
     * PUT Method with Access Token
     * @param url
     * @param inputbody
     * @param accesstoken
     */

    public  putBodyWithAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);

        this._results = got( url, {
            headers: {

                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'PUT',
            body: inputbody
        } );
        return this._results;
    }

    /**
     * PATCH Method with Access Token
     * @param url
     * @param accesstoken
     */

    public  patchBodyWithAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);

        this._results = got( url, {
             headers: {

                'Authorization': accesstoken,
                 'Content-Type': 'application/json',
            },
            method: 'PATCH',
            body: inputbody
        } );
        return this._results;
    }

    /**
     * DELETE Method with Access Token
     * @param url
     * @param accesstoken
     */
    public  deleteRequestWithAccessToken(url, accesstoken) {
        this._results =  got(url, {  headers: {

                Authorization: accesstoken,
            },
        });
        return this._results;

    }

    /**
     * Delete with Body and Access Token
     * @param url
     * @param inputbody
     * @param accesstoken
     */

    public  deleteRequestWithBodyandAccessToken(url, inputbody, accesstoken) {
        const obj = JSON.parse(inputbody);

        this._results = got( url, {
            headers: {

                'Authorization': accesstoken,
                'Content-Type': 'application/json',
            },
            method: 'DELETE',
            body: inputbody
        } );
        return this._results;
    }


 public validateResponseWithJsonPath(response, responseFields) {

        const responsemap = responseFields.rowsHash();
        console.log(responseFields);
        console.log(responsemap);
        const json = JSON.parse(response.body);
        const userStr = JSON.stringify(responsemap);
        console.log('response fields from feature file' + userStr);
        JSON.parse(userStr, (key1, value1) => {
            console.log(' key1 :' + key1 + 'value 1 :' + value1 );
            if (value1 instanceof Object === false) {
                expect(eval('json.' + key1).toString()).to.equals(value1.toString());
            }
        });
    }
    public validateResponse(response, responseFields) {
        const responsemap = responseFields.rowsHash();
        const userStr = JSON.stringify(responsemap);
        console.log('Expected response as json string' + userStr);
        let expectedvalue: boolean = false;
        JSON.parse(userStr, (key1, value1) => {
            if (value1 instanceof  Object === false) {
                expectedvalue = false;
                JSON.parse(response.body, (key2, value2) => {
                    if (key1 == key2 && value1 == value2) {
                        expectedvalue = true;
                        assert.strictEqual(value1.toString(), value2.toString(), key1  + ' or ' + value1  +
                            ' is not present in response');
                    }
                });
                if ( expectedvalue === false ) {
                    assert.strictEqual(false, true, key1  + ' or ' + value1  +
                        ' is not present in response');
                }
            }
        });

    }
}
