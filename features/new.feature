

  Scenario : generate access token
     When send POST request to "https://idm.dev.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token", input as form body is
  """
  {
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
  }
  """
  Then the status code must be 200

 # Scenario: Get users list with limit

  #   Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=20&offset=0"
   #  Then user response includes the following
    # | securityRoleId                                                           | 99                     |
     #| phoneNumber                                                              | 122-222-2222           |