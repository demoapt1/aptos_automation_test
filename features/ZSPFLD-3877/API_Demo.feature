Feature: Aptos API Demo feature

  Background: : Scenario demo
    Given get access token from "/auth/realms/aptos-denim_default/protocol/openid-connect/token", input body is
    """
     {
    "grant_type":"password",
    "username":"idm.username",
    "password":"idm.password",
    "client_id":"localhost-dev"
     }
    """

  @createUser
  Scenario: create user
    When I create a user in the application by using input as
      """
     {
      "username": "user_name",
      "password": "1234",
      "firstName": "Swati",
      "lastName": "Shikhar",
      "accountStatus": "Active",
      "phoneNumbers": [
      {
        "typeCode": "home",
        "phoneNumber": "122-222-2222"
      }
      ]
     }
     """
    Then the status code will be 200

  @GetUserList
  Scenario: To Get the List of Users
    When I get users list
    Then the status code will be 200
