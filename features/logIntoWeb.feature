Feature: log in to the webUrl

  @UISmoke
  Scenario Outline:  APTOS Device ID Validations

    Given User launch APTOS "<URL>" and navigated to APTOS Login Screen
    When User enter "<Username>" and "<Password>"
    Then User navigate to EJViewer and select "<Store>" and Enter "<DeviceId>"

    Examples:
      |URL|Username|Password|Store|DeviceId|
      |https://apps.test.aptos-denim.aptos-labs.io/ejviewer|siris|Welcome@1234|Chicago, Aptos Store|139_001|
