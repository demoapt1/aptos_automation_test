Feature: Get book by ISBN


  Scenario: User calls web service to get a book by its ISBN
	Given send GET request to "https://www.googleapis.com/books/v1/volumes", with an isbn of 9781451648546
	Then the status code is 200
    Then user response includes the following
    | totalItems                                                           | 1                       |
    | kind                                                                 | books#volumes           |
    | publisher                                        | Simon and Schuster      |
    | identifier                | 1451648545              |


Scenario: Make sample post call

Then send POST request to "http://httpbin.org/post", input body is
"""
{
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
 }
""""

Scenario: Get users list

  Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=20&offset=0"
  Then user response includes the following
    | securityRoleId                                                           | 99                     |
    | phoneNumber                                                              | 122-222-2222         |