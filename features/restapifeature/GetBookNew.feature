Feature: Get User List


  Scenario: Generating access token
	Given send POST request to "https://idm.dev.aptos-denim.aptos-labs.io/auth/realms/aptos-denim_default/protocol/openid-connect/token", input body is
    """
     {
    "grant_type":"password",
    "username":"swati.shikhar",
    "password":"Zensar@123",
    "client_id":"localhost-dev"
     }
    """
    Then the status code must be 200

  Scenario: Get particular user from users list

    Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users/cdcd8598-46f2-4bec-8167-de3b800b0bde"

      Then user response includes the following
        | username                                                                 | 1111                 |
        | phoneNumber                                                              | 122-222-2222         |
        |securityRoleId                                                            | 10                   |
        |userId                                                                    | cdcd8598-46f2-4bec-8167-de3b800b0bde|
     Then response includes the following
       | username                                                                  | 1111                 |
       | phoneNumbers[0].phoneNumber                                               | 122-222-2222         |
       |retailLocationRoles[0].securityRoleId                                      |10                    |
       |externalIds[0].userId                                                      |cdcd8598-46f2-4bec-8167-de3b800b0bde|

  Scenario: Get users list with limit

     Given send GET request to "https://api.dev.aptos-denim.aptos-labs.io/users/v2/users?limit=5&offset=0"

     Then user response includes the following
      | username                                                                 | 1111                 |
      | phoneNumber                                                              | 122-222-2222         |
      |securityRoleId                                                            | 10                   |

     Then response includes the following
       | data[3].username                                                          | 1111                 |
       | data[3].phoneNumbers[0].phoneNumber                                       | 122-222-2222         |
       |data[3].retailLocationRoles[0].securityRoleId                              | 10                    |